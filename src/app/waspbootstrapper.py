import requests
import json
import os

class WASPBootstrapper():
    def __init__(self, socketio=None, api_gateway_admin_url=os.getenv("APIGATEWAY_ADMIN_INTERNAL_BASEURL")):
        self.sio = socketio
        self.repos_checked_out = False
        self.api_gateway_admin_url = api_gateway_admin_url
        self.guest_consumer_id = None

    def bootstrap(self):
        print("SIO", self.sio)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Start bootstrapping"})
        except Exception:
            pass
        self.enableCorrelationIdPlugin()
        # self.enableBasicAuthPlugin()
        self.enableCorsPlugin()
        # self.enableAclPlugin()
        #self.createRootConsumer()
        #self.createGuestConsumer()
        #self.createDemoConsumer()
        #self.createSystemConsumer()
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Bootstrapping finished"})
        except Exception:
            pass
        return True

    def enableCorrelationIdPlugin(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Enabling correlationId plugin..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/plugins"
        payload = json.dumps({
            "name": "correlation-id",
            "config": {
                "header_name": "X-Correlation-ID",
                "generator": "uuid",
                "echo_downstream": True
            }
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"CorrelationId-plugin enabling gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def enableCorsPlugin(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Enabling cors plugin..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/plugins"
        payload = json.dumps({
            "name": "cors",
            "config": {
                "origins": ["*"],
                "credentials": True,
                "max_age": 86400,
                "headers": ["Authorization", "Content-Type", "apikey", "cache-control"]
            }
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Cors-plugin enabling gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def enableBasicAuthPlugin(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Enabling BasicAuth plugin..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/plugins"
        payload = json.dumps({
            "name": "basic-auth",
            "config": {
                "hide_credentials": True,
                "anonymous": self.guest_consumer_id
            }
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"CorrelationId-plugin enabling gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def enableAclPlugin(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Enabling Acl plugin..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/plugins"
        payload = json.dumps({
            "name": "acl",
            "config": {
                "deny": ["banned"]
            }
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Acl-plugin enabling gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def createBasicAuthCredential(self, consumer, username, password):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Creating BasicAuth credentials for {username} plugin..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + f"/consumers/{consumer}/basic-auth"
        payload = json.dumps({
            "username": str(username),
            "password": str(password)
        })
        headers = {
            'Content-Type': 'application/json'

        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"BasicAuth Creation for {username} gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def addGroupToConsumer(self, consumer, group):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Adding group {group} to {consumer}"})
        except Exception:
            pass
        url = self.api_gateway_admin_url + f"/consumers/{consumer}/acls"
        payload = json.dumps({
            "group": str(group)
        })
        headers = {
            'Content-Type': 'application/json'

        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Adding group {group} to {consumer} gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def createRootConsumer(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Creating root-consumer..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/consumers"
        payload = json.dumps({
            "username": "root",
            "custom_id": "ef842338-9e94-4d95-8cfe-ac47dc824516",
            "tags": [
                "role_root",
                "grant_everything"
            ]
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Root-consumer-creation gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def createGuestConsumer(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Creating guest-consumer..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/consumers"
        payload = json.dumps({
            "username": "guest",
            "custom_id": "7a61e63a-3d75-487c-9027-77e6f766d6bf",
            "tags": [
                "role_guest",
                "grant_guest"
            ]
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        self.guest_consumer_id = response.json().get("id")
        consumer = response.json().get("id")

        self.createBasicAuthCredential(consumer, "guest", "guest")
        self.addGroupToConsumer(consumer=consumer, group="guest")
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Guest-consumer-creation gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

    def createDemoConsumer(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Creating demo-consumer..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/consumers"
        payload = json.dumps({
            "username": "demo",
            "custom_id": "259550f9-2dba-4237-af77-a464640dec9d",
            "tags": [
                "role_demo",
                "grant_demo"
            ]
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Demo-consumer-creation gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

        consumer = response.json().get("id")
        self.createBasicAuthCredential(consumer, "demo_1", "demo_1")
        self.createBasicAuthCredential(consumer, "demo_2", "demo_2")
        self.createBasicAuthCredential(consumer, "demo_3", "demo_3")
        self.addGroupToConsumer(consumer=consumer, group="demo")
        self.addGroupToConsumer(consumer=consumer, group="user")

    def createSystemConsumer(self):
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"Creating system-consumer..."})
        except Exception:
            pass
        url = self.api_gateway_admin_url + "/consumers"
        payload = json.dumps({
            "username": "system",
            "custom_id": "2f749f66-ac02-49b9-bdf5-154ffc6d97df",
            "tags": ["root"]
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        try:
            if self.sio is not None and self.sio.connected:
                self.sio.emit('addLog', {"message": f"System-consumer-creation gateway response (Code " + str(
                    response.status_code) + " ): <pre>" + response.text + "</pre>"})
        except Exception:
            pass

        consumer = response.json().get("id")
        self.createBasicAuthCredential(consumer, os.getenv("SYSTEM_USER"), os.getenv("SYSTEM_PASSWORD"))
        self.addGroupToConsumer(consumer=consumer, group="root")
        self.addGroupToConsumer(consumer=consumer, group="user")

