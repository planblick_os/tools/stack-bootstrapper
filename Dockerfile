FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

COPY src /src

RUN chmod 777 /src/runApp.sh
RUN chmod +x /src/runApp.sh
WORKDIR "/src/app"
CMD ["/src/runApp.sh"]
